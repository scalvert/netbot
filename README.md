netbot lets you skip the terminal when gathering some simple network info. the app listens to channels specified through the Slack GUI using the outgoing webhooks integration.
netbot will direct message the user who uses the command in question with the results when they are available.

netbot will respond to slack messages in the approved channel in the following cases:

whohosts -
	will return an attempt to parse whois output for the hosting company
	e.g. whohosts netflix.com returns
	Amazon Technologies

portscan -
	will perform a quick port scan of the host given as a second argument and return a list of open ports
	e.g. ports samcalvert.co might return
	22, 80, 443
