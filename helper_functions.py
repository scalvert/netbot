# Functions to make netbot run
import socket
import subprocess
import nmap

def generate_imdict(sc):
    # sc is a slackclient object. returns a dictionary where the keys are user IDs and the values are IM ids
    # this can be used for a dict to be able to send users direct messages from netbot
    im = sc.api_call("im.list")
    if im['ok']:
        imlist = im['ims']
        imdict={}
        for profile in imlist:
            imdict[profile['user']] = profile['id']
        return imdict
    else:
        return False 


def send_message(sc,channel_id, message):
    # sc is s slackclient object, channel_id is the slack channel or user id to the send the message to.
    sc.api_call(
            "chat.postMessage",
            channel=channel_id,
            text=message,
        )

def getdomain(text):
    # parse the text portion of the request form to extract the domain.
    # todo: harden / filter this text input
    print text
    if len(text.split()) != 2:
        #print (text, "Does not follow usage. Usage = whohosts [domain.tld]")
        return False
    else:
        domain = text.split('|')[1][:-1]
        return domain

def resolve(domain):
    # attempts to resolve a domain to an IPv4 address; returns a list where the first element is
    # whether the attempt was successful as a bool, the second element is a string describing the result
    try:
        print "trying to resolve ", domain, "..."
        ip = socket.gethostbyname(domain)
        print ip
    except:
        return [False, ("Could not resolve " + domain)] 
        raise
    return [True, ip]

def whohosts(ip):
    # get whois info as parsed by the iphost bash script
    # todo: add more resilience for unusual whois returns
    whois = subprocess.check_output(["./iphost.sh", ip])
    return whois

def is_valid_ip(ip):
    # determine if an IP is valid. if not, try to resolve it as a host name
    # don't allow localhost scans
    if ip == '127.0.0.1' or ip == 'localhost':
        return [False, "Forbidden"]
    else:
        try:
            socket.inet_aton(ip)
            return [True, ip]
        except socket.error:
            return [False, "Not a valid IP"]

def portscan(ip):
    #validate = is_valid_ip(ip)
    #if validate[0]:
    #    ip = validate[1]
    #else:
    #    return validate[1]
    nm = nmap.PortScanner()
    nm.scan(ip, '20-9000', '-T5')
    output = ""
    for host in nm.all_hosts():
        output += '\n' + ('----------------------------------------------------')
        output += '\n' + ('Host : %s (%s)' % (host, nm[host].hostname()))
        output += '\n' + ('State : %s' % nm[host].state())
        for proto in nm[host].all_protocols():
            output += '\n' + ('----------')
            output += '\n' + ('Protocol : %s' % proto)

            lport = nm[host][proto].keys()
            lport.sort()
            for port in lport:
                output += '\n' +  ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))
    return output


#print portscan('159.203.88.121')
