import os
import subprocess
import socket
from helper_functions import *
from flask import Flask, request, Response
from slackclient import SlackClient

slack_token = os.environ["BOT_TOKEN"]
sc = SlackClient(slack_token)
app = Flask(__name__)

SLACK_WEBHOOK_SECRET = os.environ.get("WEBHOOK_SECRET")

imdict = generate_imdict(sc)

whohosts_disclaimer = """ _whohosts parses the output of a unix whois command for orgname and role. If that fails, it attempts to return the abuse email address._ """
@app.route('/slack', methods=['POST'])
def inbound():
    # Check that request token matches slack's secret. If so, parse the request text and return WHOIS info
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        #print(request.form)
        username = request.form.get('user_name')
        trigger = request.form.get('trigger_word')
        text = request.form.get('text')
        userid = request.form.get('user_id')
        im = imdict[userid]
        #inbound_message = username + " in " + channel + " says: " + text
        #print(inbound_message)
        if trigger == 'whohosts':
            domain = getdomain(text)
            print domain
            ip = resolve(domain)
            print ip
            if ip[0]: # check whether the whohosts function was able to resolve the IP address in question
                host = whohosts(ip[1])
                send_message(sc, imdict[userid], (whohosts_disclaimer + "\n" + domain + " resolves to " + ip[1] + " ; host info: " + "\n" + "```" + host + "```"))
            else:
                print ip
                send_message(sc, im, (domain + " could not be resolved"))
        elif trigger == 'portscan':
            target = text.split()[1]
            validate = is_valid_ip(target) # check if input is a valid IP if not, try to resolve it
            if validate[0]:
                ip = target[1]
            elif validate[1] == 'Forbidden':
                send_message(sc, im, "operation not permitted")
            else:
                domain = getdomain(text)
                ip = resolve(domain)
                if ip[0]:
                    send_message(sc, im, ("Scanning " + target + " ..."))
                    scan = portscan(ip[1])
                    send_message(sc, im, ("```" + scan + "```"))
                else:
                    send_message(sc, im, (ip[1]))
    return Response(), 200


@app.route('/', methods=['GET'])
def test():
    return Response('It works!')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
