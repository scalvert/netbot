#!/bin/bash

# takes an IP address as an argument, returns a reasonable guess as to who hosts the IP address
# the guess is based on parse whois informatoin which is notoriously un-uniform
# in tests, the grepped parameters here get the right response most of the time.
# In cases where the egrep gets no response, the script attempts to return an abuse email address 

IP=$1

HOST=`whois $IP | egrep -i 'orgname|org-name|role'`
if [$HOST == '']
       then
	       whois $IP | grep -i Abuse
       else
	       echo $HOST	      
fi
